package com.sassposible.testberita.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.sassposible.testberita.R;
import com.sassposible.testberita.adapter.Adapter_List_News;
import com.sassposible.testberita.response.ArticlesItem;
import com.sassposible.testberita.response.Response;
import com.sassposible.testberita.response.Source;
import com.sassposible.testberita.retrofit.APIClient;
import com.sassposible.testberita.retrofit.APIService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
    Adapter_List_News myAdapter;
    RecyclerView recyclerView;
    ArrayList<ArticlesItem> articlesItems = new ArrayList<>();;
    Boolean loadMore = false;
    int press = 0;
    String[] title,poster,urls,content,publishedAt;
    Source[] sources;
    SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        sliderLayout = findViewById(R.id.slider);
        recyclerView = findViewById(R.id.rvNews);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new Adapter_List_News(MainActivity.this,recyclerView,articlesItems);
        recyclerView.setAdapter(myAdapter);
        getArticle();

    }

    public void getArticle(){
        if (loadMore == false) {
            articlesItems.clear();
        }
        final String url = "http://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=da5bdc26331345d4be3d60903f8131ec";
        APIService service = APIClient.getClient().create(APIService.class);
        Call<Response> getArtilce = service.getArticle(url);
        getArtilce.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    List<ArticlesItem> list = response.body().getArticles();
                    HashMap<String,String> url_banner = new HashMap<String, String>();
                    if (list.size() != 0) {
                        title = new String[list.size()];
                        poster = new String[list.size()];
                        sources = new Source[list.size()];
                        urls = new String[list.size()];
                        content = new String[list.size()];
                        publishedAt = new String[list.size()];

                        for (int i = 0; i < list.size(); i++) {
                            title[i] = list.get(i).getTitle();
                            poster[i] = list.get(i).getUrlToImage();
                            sources[i] = list.get(i).getSource();
                            urls[i] = list.get(i).getUrl();
                            content[i] = list.get(i).getContent();
                            publishedAt[i] = list.get(i).getPublishedAt();
                            if (i<5) {
                                url_banner.put(""+title[i],""+poster[i]);
                            }
                            ArticlesItem articlesItem = new ArticlesItem(title[i],publishedAt[i],poster[i],urls[i],content[i],sources[i]);
                            articlesItems.add(articlesItem);

                        }
                        recyclerView.setAdapter(myAdapter);
                        loadMore = false;
                        myAdapter.notifyDataSetChanged();
                        myAdapter.setLoaded();

                        for(String name : url_banner.keySet()){
                            TextSliderView textSliderView = new TextSliderView(MainActivity.this);

                            // initialize a SliderLayout
                            textSliderView
                                    .image(url_banner.get(name))
                                    .setScaleType(BaseSliderView.ScaleType.Fit);

                            //add your extra information
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle()
                                    .putString("extra",name);

                            sliderLayout.addSlider(textSliderView);
                        }
                        loadMore = false;
                        myAdapter.notifyDataSetChanged();
                        myAdapter.setLoaded();

                    }else {
                        Toast.makeText(getApplicationContext(),"0", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"eror response", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail to get news", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (press==0) {
            Toast.makeText(getApplicationContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
            press = 1;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    press = 0;
                }
            }, 3000);
        }else{
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            press = 0;
        }

    }
}
