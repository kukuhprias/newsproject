package com.sassposible.testberita.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sassposible.testberita.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class DetailNews extends AppCompatActivity {
    ImageView imgPoster;
    TextView txtTitle,txtSource,txtDate,txtContent,txtGotoAricle;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back));

        imgPoster = findViewById(R.id.imgPoster);
        txtTitle = findViewById(R.id.txtTitle);
        txtContent = findViewById(R.id.txtContent);
        txtDate = findViewById(R.id.txtDate);
        txtSource = findViewById(R.id.txtSource);
        txtGotoAricle = findViewById(R.id.txtGotoAricle);

        Picasso.with(DetailNews.this)
                .load("" + getIntent().getStringExtra("poster"))
                .noPlaceholder()
                .fit()
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(imgPoster);
        txtTitle.setText(getIntent().getStringExtra("title"));
        txtSource.setText("Sumber : "+getIntent().getStringExtra("source"));
        txtDate.setText(getIntent().getStringExtra("date"));
        if (Objects.equals(getIntent().getStringExtra("content"), null)){
            txtContent.setText("Tidak ada content yang tersedia silahkan klik baca artikel asli");
        }else {
            txtContent.setText(getIntent().getStringExtra("content"));
        }
        txtGotoAricle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailNews.this, WebView.class)
                        .putExtra("url",""+getIntent().getStringExtra("url")));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
