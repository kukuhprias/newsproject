package com.sassposible.testberita.service;

public interface OnLoadMoreListener {
    void onLoadMore();
}
