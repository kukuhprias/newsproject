package com.sassposible.testberita.retrofit;

import com.sassposible.testberita.response.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface APIService {
    @GET
    Call<Response> getArticle(@Url String url);
}
