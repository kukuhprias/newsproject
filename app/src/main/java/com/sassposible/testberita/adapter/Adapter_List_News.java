package com.sassposible.testberita.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sassposible.testberita.activity.DetailNews;
import com.sassposible.testberita.R;
import com.sassposible.testberita.response.ArticlesItem;
import com.sassposible.testberita.service.OnLoadMoreListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

public class Adapter_List_News extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final int VIEW_TYPE_ITEM = 0;//variable for condition visible item list
    private final int VIEW_TYPE_LOADING = 1;//variable for condition visible loading load more

    private OnLoadMoreListener mOnLoadMoreListener;//variable for constructor loadmore

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    Context c;
    private ArrayList<ArticlesItem> articlesItems;

    public Adapter_List_News (Context c,RecyclerView mRecyclerView,ArrayList<ArticlesItem> articlesItems){
        this.articlesItems = articlesItems;
        this.mRecyclerView = mRecyclerView;
        this.c = c;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.model_news, parent, false);
            return new Adapter_List_News.ArticleViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new Adapter_List_News.LoadingViewHolder(view);
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof Adapter_List_News.ArticleViewHolder) {
            final ArticlesItem articlesItem = articlesItems.get(position);
            Adapter_List_News.ArticleViewHolder articleViewHolder = (Adapter_List_News.ArticleViewHolder) holder;
            Picasso.with(c)
                    .load("" + articlesItem.getUrlToImage())
                    .noPlaceholder()
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(articleViewHolder.imgPoster);
            articleViewHolder.txtTitle.setText(articlesItem.getTitle());
            articleViewHolder.txtSource.setText("Sumber : "+articlesItem.getSource().getName());
            Locale Indonesia = new Locale("id","ID");
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'",Indonesia);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("EEEE dd MMM yyyy",Indonesia);
            LocalDate date = LocalDate.parse(""+articlesItem.getPublishedAt(), inputFormatter);
            final String formattedDate = outputFormatter.format(date);
            articleViewHolder.txtDate.setText(formattedDate);
            articleViewHolder.txtContent.setText(articlesItem.getContent());
            articleViewHolder.lineArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    c.startActivity(new Intent(c, DetailNews.class)
                    .putExtra("poster",""+articlesItem.getUrlToImage())
                    .putExtra("title",""+articlesItem.getTitle())
                    .putExtra("content",""+articlesItem.getContent())
                    .putExtra("date",""+formattedDate)
                    .putExtra("source",""+articlesItem.getSource().getName())
                    .putExtra("url",""+articlesItem.getUrl()));
                }
            });
        }
    }

    public int getItemCount() {
        return articlesItems == null ? 0 : articlesItems.size();
    }


    public int getItemViewType(int position) {
        return articlesItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder{
        ImageView imgPoster;
        TextView txtTitle,txtSource,txtDate,txtContent;
        LinearLayout lineArticle;

        public ArticleViewHolder(@NonNull View view) {
            super(view);
            c = view.getContext();
            imgPoster = view.findViewById(R.id.imgPoster);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtSource = view.findViewById(R.id.txtSource);
            txtDate = view.findViewById(R.id.txtDate);
            txtContent = view.findViewById(R.id.txtContent);
            lineArticle = view.findViewById(R.id.lineArticle);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

}
