# NewsProject

NewsProject dibuat menggunakan android studio 3.5.3
Aplikasi ini dapat berjalan pada android 4.4(kitkat) keatas, dan sudah di uji pada android 9(Pie).

aplikasi ini menggunakan berbagai library antara lain

    implementation 'com.google.android.material:material:1.0.0'(untuk menambah material component pada android ui xml)
    implementation 'com.android.support:design:28.0.0' (menambahkan dukungan untuk berbagai komponen dan pola desain material)
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'(untuk mendukung kompatibilitas pada versi android yang lebih lama)
    implementation 'com.nineoldandroids:library:2.4.0'(menambahkan animasi pada image slider)
    implementation 'com.daimajia.slider:library:1.1.5@aar'(menambahkan image slider)
    implementation 'com.squareup.retrofit2:retrofit:2.0.2'(menambahkan retrofit component untuk memparsing json/gson)
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'(library tambahan retrofit gson converter)
    implementation 'com.google.code.gson:gson:2.6.2'(library gson tambahan dari google)
    implementation 'com.squareup.picasso:picasso:2.5.0'(untuk menampilkan gambar dari url https)

terimakasih